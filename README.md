# C++ CODING GUIDELINE

## Labs DevMentor.org Corp.

## Introduction

This guideline was designed to make searching through code more effective. It brings uniformity and consistency, so code is more easier to read.

## Self Documenting

Write code that is self descriptive, such that a developer is able to infer very quickly what a function, code block, or section of the code is doing. If the purpose of the code is not clear, you need to carefully think of the name and leading comments.

## File Naming

* Header file must use the extension .hpp
* Source file must use the extension .cpp
* Source file with main entry point should have extension, .main.cpp
* Don't use the impl idiom or separate .impl files.

## Folders

* Put source files in _src_ folder under project root.
* Put project header files in _include_ folder under _src_.
* Put 3rd party header files in _include_ folder under project root.
* All build and compile artifacts should go under the _build_ folder.
* Library files should go under a _lib_ folder.
* Under the _src_ folder must exist a _test_ folder (all source file must have unit tests).
* Project documents should be in their own _docs_ folder.

```
   root /
        |- build/
        |- docs/
        |- include/
        |- lib/
        |- src/
            |- include/
            |- test/
```

* All header and source files MUST contain a link to this coding guidelines!
* Header file should have a header guards.
* A header file must not have any includes (excessive includes increase compile time).
  * Very rare cases where forward declaration can't be used may include a header.
* For sub-classing, use forward declaration (Do not use include in header file).

* In source file make sure headers are declared in the following order.
  * All source file should have a header file, even if it's empty,
  * The order of the includes should be (top to bottom):

    1. System header first (C than C++).
    1. 3rd party headers.
    1. Project header.

    **Note**: Group system header together, then 3rd party, and project header and separate with a single empty line.

    **Note**: When including old system c style headers use the namespace format. Eg. <cstdio>

```C++
// SYSTEM
#include <string>
#include <vector>

// 3RD PARTY
#include <asio.hpp>
#include <boost/shared_ptr.hpp>

// PROJECT
#include "core.hpp"
#include "utils.hpp"
```

* Do not nest conditions in a if statement, makes debugging annoying!

```C++
// Don't do this, no error checking on get_buffer_name, buggy-smelly code!
if( open( get_buffer_name() ) )

// Do this, don't worry about extra variable
char* buf = get_buffer_name();
if( buff && open( buff ) )
```

## Comments

* Comment code well, all comments should be on a line of their own, before code or any declaration.

* Always write clearly all the assumption you are making when writing code, or making updates to it. This will aid anyone else who might have to come in after to fix a bug.

* Follow the Doxygen comment guidelines.

* Every 10 lines of code should have a comment, explaining what is suppose to occur (not the how).

  * Code should have logical group, separated by blank line.
  * It is easer to read one line of comment than several lines of code to make out what is happening!

## Naming

* All variable names are lower snake cased (word separated with underscore)

```C++
char buffer_type_default = '';
```

* Variable name must be descriptive.

* Variable name should follow noun + verb + type (verb & type parts are optional).
  * Noun = name ( eg. object, subject eg: buffer, socket, file ).
  * Verb = action( eg. open, close, reset ).
  * Type = an attribute( eg. start, end ).

  * A variable name can fall into one of these groups:

    1. Noun only
    1. Noun + verb
    1. Noun + type
    1. Noun + verb + type
    1. Verb only
    1. Verb + type
    1. Type only

    **Note**: This will aid in being able to grep code and narrow your search down.

* Single variable declaration only, one per line.

```C++
int a;
int b;

//DON'T DO THIS
int a,b,c;
```

* Function name should follow same format as variable name.

  * Noun + Verb + Type (where verb & type are optional).

  * It is much easier to group search, if you want to find all functions that operator on a buffer this will be made simple by the fact name starts with a noun.

**Searching for "buffer*" could result in.**

```C++
buffer_open(...)
buffer_read(...)
buffer_write(...)
buffer_copy(...)
buffer_close(...)
```

* Function name should use snake case and be in lower code.

* When referring a count or size use an unsigned type like size_t.

  Unless there is a need to use negative values, don't use signed types int, long.

* Do not pass a raw pointer around!
  1. Use a reference when you can (avoid null checks).
  1. Use a std::scoped_ptr for local scope (prevent memory leak).
  1. Use a std::shared_ptr to pass around (prevent memory leak).

## Functions

* Function parameter ordering:
  1. Error code, out parameter by reference is always first (type error_t ).
  1. IN parameters.
  1. OUT parameters.
  1. IN/OUT parameters.
  1. All input parameters should appear last (must be const)
  1. Return bool success status (true - pass, false - fail).

```C++
typedef unsigned long error_t;

bool fn( error_t & err,
         string & o_data,
         int & d_bufsize,
         int i_mode = MODE_COPY );
```

  This will keep functions uniform, provide rich error code. The boolean error return code allows method chaining. Having input parameters at the end allows use of default parameter values.

  This way we can write better function and deal with failure much better, here is an improved version of the atoi function.

  Function that does not require error reporting may omit the error parameter and have a return type of **void**.

```C++
error_t err;
int i;
if( ! atoi(err, str, i) )
{
   // Use a safe default value
   i = 18;
}
```

* Parameter naming conventions:

|Param Type|Description|Usage|
|-----|-----------|-----|
|error |Should be named err| |
|in    |Prefix arg with 'i_'|void foo( const int i_count )|
|out   |Prefix arg with 'o_'|void bar( char& o_address, size_t& o_size )|
|in/out|Param prefix arg with 'd_'|void fetch( char& o_buffer, int& d_size )|

    **Note**: d_ stands for duplex as a way to remember.

* All 'in' parameters must be type **const**, they should never be modified inside the function body.

## Error Reporting

```C++
bool func( error_t & err, char * d_buffer );
```

* Function return, use bool to indicate success/fail of ALL functions.

  * Use an integer out parameter for rich error based on proper error codes.
  * More flexible error handling, use true/false for pass/catch-all error.
  * Use error code to handle specific error conditions.

### Example 1 - Proper Format

```C++
bool get_buffer( error_t & err, char * o_buf );
bool read( error_t & err, char * i_buff );

int err_code;
char* buf = 0;
if( get_buffer( err_code, buf ) )
{
   read( err_code, buf );
}
else if( err_code == NOT_READY )
{
}
else if( err_code == CLOSE )
{
}
else
{
  // Undefined error code, or catch-all
}
```

### Example 2 - Improper Format

```C++
// Run of the mill, mixed semantic return value.
// Don't code your function like this.
int get_member_count();

// How is an error handled?
if( get_member_count() > 10 )
```

### Example 3 - Proper Format

```C++
// Error interface simple, consistent, rich error code.
// Note count should be unsigned value.
bool get_member_count( error_t & err, size_t& o_count );

int err = 0;
int count = 0;
if( get_member_count( err, count ) && count > 10 )
{
}
```

* Error code, never use zero to indicate success.

* Use of **const**
   1. Mark read-only value with const.
   1. Mark read-only function as const.

## Formatting

* Put pointer '*' and reference '&' after type, separated by a space.

```C++
char * buf = 0;
long & pos = 100;
```


* Braces on their own line.

```C++
while(...)
{
}
```

* Single line if statement should use braces.

```C++
if( !a )
{
    send( a );
}
```

* Checking for zero or true/false values.

```C++
// Check zero & null value this way:
if( n ) or if( !n )

// Don't do this:
if( n != 0 )
if( n == 0 )
```

* C++11: use **std::nullptr** to distinguish from integer value zero!

* Use anonymous namespace for variable to specify internal linkage(file scope) within a translation unit.

```C++
// some header file
namespace
{
   const int MAX_BUF_SIZE = 50;
}
```

## TODO

```
?? Error logging.
?? Classes designed for unit testing, use interface, use class factory.
?? Never use new.
?? Buffer overrun protection.
?? Uninitialized variables.
?? Off by one errors (indexing, looping).
?? Objects, return by value, copying, temp objects(C+11 have move semantics).
?? Don't use define, use const.
?? enums
?? class members
?? class methods
?? Header file format.
?? Source file format.
?? Error codes
?? Exceptions
```